/* eslint-disable */
jest.setTimeout(2000)

const urlTest = 'http://destination-component.locale/test.html'
const urlVendor = 'http://destination-component.locale/public/js/vendor.js'
const urlComponent = 'http://destination-component.locale/public/js/destination-component.js'

const inputSelector = 'input.selector'
const destinationSelector = 'div.selector'
const dropdown = 'destination-component_dropdown'
const placeholder = 'div.destination-component_placeholder'
const destinations = require('../config/one-level.js')
const devices = require('puppeteer/DeviceDescriptors')
const iPhonex = devices['iPhone X']
const search = 'div.destination-component_search-container input'

const settings = {
    appearance: {
        search: false,
        mode: 'columns',
        orientation: 'auto', // Auto, top, bottom
        breakpoints: {
            one: 400,
            two: 800,
        },
    },
    destinations: destinations
}


const customBeforeAll = (settings) => {
    beforeAll (async () => {
        await page.goto(urlTest)
        await page.addScriptTag({ url: urlVendor })
        await page.addScriptTag({ url: urlComponent })
        await page.evaluate((settings) => {
            const DestinationComponent = window.DestinationComponent //otherwise the transpiler will rename it and won't work
            new DestinationComponent('.selector', settings)
        }, settings)
    },)
}

describe ('Created destination selector one level', () => {

    customBeforeAll(settings)

    it('should there are hidden input', async () => {
        await page.waitForSelector(inputSelector)
        let inputElement = await page.$(inputSelector)
        await expect(page.evaluate(element => element.getAttribute("style"), inputElement)).resolves.toMatch('display: none;')
    })

    it('should have selector class', async () => {
        let destinationElement = await page.$(destinationSelector)
        await expect(page.evaluate(element => element.getAttribute("class"), destinationElement)).resolves.toMatch('selector')
    })

    it('should there are a opened dropdown element', async () => {
        let destinationElement = await page.$(destinationSelector)
        destinationElement.click()
        await page.waitFor(100)
        await expect(page.evaluate(element => element.getAttribute("class"), destinationElement)).resolves.toMatch(dropdown)
    })

    it('should be a placeholder named', async () => {
        let element = await page.$('[data-id="2"]')
        await element.click()
        await page.waitFor(100)
        let placeholderElement = await page.$(placeholder)
        await expect(page.evaluate(element => element.innerHTML, placeholderElement)).resolves.toMatch('Catalunya - Barceloneta')
    })

    it('should have to be dropdown element closed class', async () => {
        let destinationElement = await page.$(destinationSelector)
        await expect(page.evaluate(element => element.getAttribute("class"), destinationElement)).resolves.not.toMatch(dropdown)
    })

})


describe ('Created destination selector one level search true', () => {

    const settings = {
        appearance: {
            search: true,
            mode: 'columns',
            orientation: 'auto', // Auto, top, bottom
            breakpoints: {
                one: 400,
                two: 800,
            },
        },
        destinations: destinations
    }

    customBeforeAll(settings)

    it('should there are hidden input', async () => {
        await page.waitFor(100)
        await page.waitForSelector(inputSelector)
        let inputElement = await page.$(inputSelector)
        await expect(page.evaluate(element => element.getAttribute("style"), inputElement)).resolves.toMatch('display: none;')
    })

    it('should have selector class', async () => {
        let destinationElement = await page.$(destinationSelector)
        await expect(page.evaluate(element => element.getAttribute("class"), destinationElement)).resolves.toMatch('selector')
    })

    it('should there are a opened dropdown element', async () => {
        let destinationElement = await page.$(destinationSelector)
        destinationElement.click()
        await page.waitFor(100)
        await expect(page.evaluate(element => element.getAttribute("class"), destinationElement)).resolves.toMatch(dropdown)
    })

    it('should type in search and show result', async () => {
        await page.type(search, 'Catalunya - Barceloneta')
        await page.waitFor(1000)
    })

    it('should be a placeholder named', async () => {
        let element = await page.$('[data-id="2"]')
        await element.click()
        let placeholderElement = await page.$(placeholder)
        await expect(page.evaluate(element => element.innerHTML, placeholderElement)).resolves.toMatch('Catalunya - Barceloneta')
    })

    it('should have to be dropdown element closed class', async () => {
        let destinationElement = await page.$(destinationSelector)
        await expect(page.evaluate(element => element.getAttribute("class"), destinationElement)).resolves.not.toMatch(dropdown)
    })

})

describe ('Created destination selector one level preselected value', () => {

    const settings = {
        appearance: {
            search: true,
            mode: 'columns',
            orientation: 'auto', // Auto, top, bottom
            breakpoints: {
                one: 400,
                two: 800,
            },
        },
        destinations: destinations,
    }

    beforeAll (async () => {
        await page.emulate(iPhonex)
        await page.goto(urlTest)
        await page.addScriptTag({ url: urlVendor })
        await page.addScriptTag({ url: urlComponent })
        await page.evaluate((settings) => {
            const DestinationComponent = window.DestinationComponent //otherwise the transpiler will rename it and won't work
            const destinations = new DestinationComponent('.selector', settings)
            destinations.setDestination({
                id: 1,
                type: 'establishment'
            })
        }, settings)
    },)

    it('should be a placeholder named', async () => {
        let placeholderElement = await page.$(placeholder)
        await expect(page.evaluate(element => element.innerHTML, placeholderElement)).resolves.toMatch('Catalunya - Costa Brava')
    })

    it('should be opened class in body', async () => {
        let destinationElement = await page.$(destinationSelector)
        destinationElement.click()
        await page.waitForSelector('body')
        let bodySelector = await page.$('body')
        await page.waitFor(500)
        await expect(page.evaluate(element => element.getAttribute("class"), bodySelector)).resolves.toMatch('destination-component--opened')
    })

    it('should not be opened class in body', async () => {
        let closeElement = await page.$('.destination-component-close')
        closeElement.click()
        await page.waitForSelector('body')
        let bodySelector = await page.$('body')
        await page.waitFor(500)
        await expect(page.evaluate(element => element.getAttribute("class"), bodySelector)).resolves.not.toMatch('destination-component--opened')
    })

    it('should there are a opened dropdown element', async () => {
        let destinationElement = await page.$(destinationSelector)
        destinationElement.click()
        await page.waitFor(100)
        await expect(page.evaluate(element => element.getAttribute("class"), destinationElement)).resolves.toMatch(dropdown)
    })

    it('should type in search and show result', async () => {
        await page.type(search, 'Llei')
        await page.waitFor(1000)
    })

    it('should be a placeholder named', async () => {
        let element = await page.$('[data-id="3"]')
        await element.click()
        let placeholderElement = await page.$(placeholder)
        await expect(page.evaluate(element => element.innerHTML, placeholderElement)).resolves.toMatch('LLeide')
    })

})


describe ('Created destination selector one level listened change event', () => {

    const settings = {
        appearance: {
            search: true,
            mode: 'columns',
            orientation: 'auto', // Auto, top, bottom
            breakpoints: {
                one: 400,
                two: 800,
            },
        },
        destinations: destinations,
    }

    beforeAll (async () => {
        await page.emulate(iPhonex)
        await page.goto(urlTest)
        await page.addScriptTag({ url: urlVendor })
        await page.addScriptTag({ url: urlComponent })
    },)


    it('should console log event', async () => {
        page.on('console', msg => {
            for (let i = 0; i < msg.args().length; ++i) {
                expect(new Promise((resolve, reject) => {
                    resolve(`${msg.args()[i]}`)
                })).resolves.toBe('JSHandle:1')
            }
        });

        await page.evaluate((settings) => {
            const DestinationComponent = window.DestinationComponent //otherwise the transpiler will rename it and won't work
            const destinations = new DestinationComponent('.selector', settings)
            destinations.selector().addEventListener('selector:changed', (ev) => {
                console.log(ev.detail.id)
            })
            destinations.setDestination({
                id: 1,
                type: 'establishment'
            })
        }, settings)
    })

})


describe ('Created destination selector one level single destination', () => {

    const settings = {
        singleDestination: true,
        appearance: {
            search: true,
            mode: 'columns',
            orientation: 'auto', // Auto, top, bottom
            breakpoints: {
                one: 400,
                two: 800,
            },
        },
        destinations: destinations,
    }

    beforeAll (async () => {
        await page.emulate(iPhonex)
        await page.goto(urlTest)
        await page.addScriptTag({ url: urlVendor })
        await page.addScriptTag({ url: urlComponent })
        await page.evaluate((settings) => {
            const DestinationComponent = window.DestinationComponent //otherwise the transpiler will rename it and won't work
            const destinations = new DestinationComponent('.selector', settings)
            destinations.selector().addEventListener('selector:changed', (ev) => {
                console.log(ev.detail.id)
            })
            destinations.setDestination({
                id: 1,
                type: 'establishment'
            })
        }, settings)
    },)


    it('should there are hidden input', async () => {
        await page.waitFor(100)
        await page.waitForSelector(inputSelector)
        let inputElement = await page.$(inputSelector)
        await expect(page.evaluate(element => element.getAttribute("style"), inputElement)).resolves.toMatch('display: none;')
    })

    it('should have destination-component-single class', async () => {
        let destinationElement = await page.$(destinationSelector)
        await expect(page.evaluate(element => element.getAttribute("class"), destinationElement)).resolves.toMatch('destination-component-single')
    })

})
