# DESTINATION COMPONENT

### USAGE

You need to create input in your html.

```javascript

npm install destination-component

```

1· Import dependencies:
```javascript

import DestinationsWidget from 'destination-component/src/js/destinations-widget'
import 'destination-component/src/css/destination-widget.css'

```
2· Init instance:
```javascript

let destinationsWidget = new DestinationsWidget(selector, settings)

```

3· Custom settings:
```javascript

const settings = {
    locale: {
        language: 'en',         // STRING
        languagesAllowed: [ 'en', 'es', 'ca', 'de', 'fr', 'ru', 'it', 'nl' ],
    },
    singleDestination: false,   // Boolean
    appearance: {
        search: false,          // Boolean
        mode: 'columns',        // columns / list
        orientation: 'auto',    // auto / top / bottom
        breakpoints: {          // Breackpoints -> columns
            one: 400,           // 0 to 400 one column
            two: 800,           // 400 to 800 two columns. More than 800 three columns
        }
    },
    destinations: [],
    // If you want to override literal defaults
    literals: {
        destination: 'Destinació |||| Destinacions',      // singular |||| plural
        no_results: 'No s’han trobat resultats que coincideixin amb "%{search}"',
        lodge: 'allotjament |||| allotjaments',
        hotel: 'hotel |||| hotels',
        bungalows: 'bungalow |||| bungalows',
    },
}

```

4· Destinations options json estructure:
```javascript

// There is three level of destination
// ONE LEVEL
const destinations = [
    {
        id: 1,
        type: 'establishment',
        establishment_type: 'hotel', // hotel / bungalows / appartment
        name: 'Hotel name',
    }
]

// TWO LEVELS
const destinations = [
     {
        id: 1,
        type: 'zone', // If there is two levels this type has to be zone
        img: 'https://picsum.photos/300/200?image=1024', // Yo can pass image to this category name
        name: 'Zone name',
        children: [
            {
                id: 1,
                type: 'establishment', 
                establishment_type: 'hotel', // hotel / bungalows / appartment
                name: 'Hotle name',
            }
        ]
    }
]

// THREE LEVELS
const destinations = [
{
        id: 1,
        type: 'zone', // If there is three levels this type has to be zone
        img: 'https://picsum.photos/300/200?image=244',
        name: 'Zone parent',
        children: [
            {
                id: 2,
                type: 'zone', // If there is three levels this type has to be zone
                name: 'zone child',
                children: [
                    {
                        id: 1,
                        type: 'establishment',
                        name: 'Hotel name',
                        establishment_type: 'hotel', // hotel / bungalows / appartment
                        category: '3',
                    }
                ]
            }
        ]
    }
]

```

### METHODS

4· Set destination:
```javascript

destinationsWidget.setDestination({
    id: 1,
    type: 'zone' // zone / establishment
})

```

### EVENTS

5· Subscribe to event:
```javascript

destinations.selector().addEventListener('selector:changed', (ev) => {
    // Event detail
    // return {id:1, type:'zone'}
    console.log(ev.detail)
})

```