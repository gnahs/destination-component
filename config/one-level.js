const destinations = [
    {
        id: 1,
        type: 'establishment',
        establishment_type: 'hotel',
        img: 'https://picsum.photos/300/200?image=244',
        name: 'Catalunya - Costa Brava',
    },
    {
        id: 2,
        type: 'establishment',
        establishment_type: 'hotel',
        img: 'https://picsum.photos/300/200?image=1024',
        name: 'Catalunya - Barceloneta',
    },
    {
        id: 3,
        type: 'establishment',
        establishment_type: 'hotel',
        img: '',
        name: 'LLeide',
    },
    {
        id: 4,
        type: 'establishment',
        establishment_type: 'hotel',
        img: 'https://picsum.photos/300/200?image=616',
        name: 'Hotels de metro',
    },
    {
        id: 5,
        type: 'establishment',
        establishment_type: 'hotel',
        name: 'Hotels de metros',
    },
    {
        id: 6,
        type: 'establishment',
        establishment_type: 'hotel',
        name: 'Hotel 6',
    },
    {
        id: 7,
        type: 'establishment',
        establishment_type: 'hotel',
        name: 'Hotel 7',
    },
    {
        id: 8,
        type: 'establishment',
        establishment_type: 'hotel',
        name: 'Hotel 8',
    },
    {
        id: 9,
        type: 'establishment',
        establishment_type: 'hotel',
        name: 'Hotel 9',
    },
    {
        id: 10,
        type: 'establishment',
        establishment_type: 'hotel',
        name: 'Hotel 10',
    },
    {
        id: 11,
        type: 'establishment',
        establishment_type: 'hotel',
        name: 'Hotel 11',
    },
]

module.exports = destinations
