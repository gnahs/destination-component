const destinations = [
    {
        id: 1,
        type: 'zone',
        img: 'https://picsum.photos/300/200?image=1024',
        name: 'Hotels de poble',
        children: [
            {
                id: 1,
                type: 'establishment',
                establishment_type: 'hotel',
                img: 'https://picsum.photos/300/200?image=244',
                name: 'Catalunya - Costa Brava',
            },
            {
                id: 2,
                type: 'establishment',
                establishment_type: 'bungalows',
                img: 'https://picsum.photos/300/200?image=1024',
                name: 'Catalunya - Barceloneta',
            },
            {
                id: 3,
                type: 'establishment',
                establishment_type: 'hotel',
                img: '',
                name: 'LLeide',
            },
        ],
    },
    {
        id: 2,
        type: 'zone',
        img: 'https://picsum.photos/300/200?image=1024',
        name: 'Hotels de ciutat',
        children: [
            {
                id: 4,
                type: 'establishment',
                establishment_type: 'hotel',
                name: 'Girona',
            },
            {
                id: 5,
                type: 'establishment',
                establishment_type: 'hotel',
                name: 'Olot',
            },
        ],
    },
    {
        id: 3,
        type: 'zone',
        img: 'https://picsum.photos/300/200?image=1024',
        name: 'Hotels de muntanya',
        children: [
            {
                id: 6,
                type: 'establishment',
                establishment_type: 'hotel',
                name: 'Girona',
            },
            {
                id: 7,
                type: 'establishment',
                establishment_type: 'hotel',
                name: 'Olot',
            },
        ],
    },
    {
        id: 1,
        type: 'establishment',
        establishment_type: 'hotel',
        img: 'https://picsum.photos/300/200?image=244',
        name: 'Catalunya - Costa Brava',
    },
    {
        id: 2,
        type: 'establishment',
        establishment_type: 'bungalows',
        img: 'https://picsum.photos/300/200?image=1024',
        name: 'Catalunya - Barceloneta',
    },
    {
        id: 3,
        type: 'establishment',
        establishment_type: 'hotel',
        img: '',
        name: 'LLeide',
    },
]

module.exports = destinations
