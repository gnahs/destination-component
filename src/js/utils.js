class Utils {
    static position(element) {
        const rect = element.getBoundingClientRect()
        return rect
    }

    static createRandomId() {
        return Math.floor((Math.random() * 1000000) + 1)
    }

    static isChild(el, tag, stop) {
        if (el.getAttribute(tag)) {
            return el
        }
        while (el.parentNode) {
            if (el.classList.contains(stop)) {
                return null
            }
            // eslint-disable-next-line
            el = el.parentNode
            if (el.getAttribute(tag)) {
                return el
            }
        }

        return el
    }

    static isMobile() {
        return (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        )
    }

    static h(nodeName, attrs, ...children) {
        const $el = document.createElement(nodeName)
        // eslint-disable-next-line
        for (const key in attrs) {
            $el.setAttribute(key, attrs[key])
        }
        children.forEach((child) => {
            if (typeof child === 'string') {
                $el.appendChild(document.createTextNode(child))
                return
            }
            if (typeof child === 'undefined') {return}
            $el.appendChild(child)
        })

        return $el
    }
}

export default Utils
