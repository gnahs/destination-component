export function getZone(findedId, destinations, index = 0, childrenIndex = 0) {
    if (Array.isArray(destinations)) {
        return getZone(findedId, destinations[index], index + 1)
    }

    const { id, type, children } = destinations

    if (id === findedId && type === 'zone') {
        return destinations
    }

    if (type === 'zone') {
        return getZone(findedId, children[childrenIndex], index, childrenIndex + 1)
    }
  
    return null
}