import Polyglot from 'node-polyglot'

// Singleton
class Translator {
    constructor(literals) {
        if (Translator.instance) {
            return Translator.instance
        }
        Translator.instance = new Polyglot({ phrases: literals })
        return Translator.instance
    }
}

export default Translator
