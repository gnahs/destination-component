const ca = {
    destination: 'Destinació |||| Destinacions',
    no_results: 'No s’han trobat resultats que coincideixin amb "%{search}"',
    lodge: 'allotjament |||| allotjaments',
    hotel: 'hotel |||| hotels',
    bungalows: 'bungalow |||| bungalows',
}

const es = {
    destination: 'Destino |||| Destinos',
    no_results: 'No se encontraron resultados que coindidan con"%{search}""',
    lodge: 'Alojamiento |||| Alojamientos',
    hotel: 'hotel |||| hoteles',
    bungalows: 'bungalow |||| bungalows',
}

const en = {
    destination: 'Destination |||| Destinations',
    no_results: 'No results matched "%{search}"',
    lodge: 'Accommodation |||| Accommodations',
    hotel: 'hotel |||| hotels',
    bungalows: 'bungalow |||| bungalows',
}

const it = {
    destination: 'Destinazione |||| Destinazioni',
    no_results: 'No results matched "%{search}"',
    lodge: 'Alloggi |||| Alloggi',
    hotel: 'hotel |||| hotel',
    bungalows: 'bungalow |||| bungalows',
}

const fr = {
    destination: 'Destination |||| Destinations',
    no_results: 'Aucun résultat ne correspond à votre recherche "%{search}"',
    lodge: 'Hébergements |||| Hébergement',
    hotel: 'hôtel |||| hôtels',
    bungalows: 'bungalow |||| bungalows',
}

const nl = {
    destination: 'Bestemming |||| Bestemmingen',
    no_results: 'No results matched "%{search}"',
    lodge: 'Accommodaties |||| Accommodaties',
    hotel: 'hotel |||| hotels',
    bungalows: 'bungalow |||| bungalows',
}

const ru = {
    destination: 'направление |||| направления |||| направления',
    no_results: 'No results matched "%{search}"',
    lodge: 'Проживание |||| Проживание',
    hotel: 'hotel |||| hotels',
    bungalows: 'bungalow |||| bungalows',
}

const de = {
    destination: 'Ziel |||| Ziele',
    no_results: 'No results matched "%{search}"',
    lodge: 'Unterkünfte |||| Unterkünfte',
    hotel: 'hotel |||| hotels',
    bungalows: 'bungalow |||| bungalows',
}

const pt = {
    destination: 'Destino |||| Destinos',
    no_results: 'No results matched "%{search}"',
    lodge: 'Alojamento |||| Alojamento',
    hotel: 'hotel |||| hotels',
    bungalows: 'bungalow |||| bungalows',
}

export default {
    ca, es, en, de, fr, it, pt, ru, nl,
}
