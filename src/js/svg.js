class Svg {
    static closeSVG() {
        return `<div class="destination-component-close">
                <?xml version="1.0" encoding="utf-8"?>
        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve">
        <g transform="translate(-284 -408.89)">
            <path d="M307,417.5l-1.6-1.6l-6.4,6.4l-6.4-6.4l-1.6,1.6l6.4,6.4l-6.4,6.4l1.6,1.6l6.4-6.4l6.4,6.4l1.6-1.6l-6.4-6.4L307,417.5z"/>
        </g>
        </svg>
        </div>`
    }

    static arrowDown() {
        return `<?xml version="1.0" encoding="utf-8"?>
        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 10 10" style="enable-background:new 0 0 10 10;" xml:space="preserve">
        <g><path d="M9.3,3.1L8.9,2.7C8.7,2.5,8.5,2.5,8.3,2.5c-0.2,0-0.4,0.1-0.5,0.2L5,5.2L2.2,2.7
                C2,2.5,1.9,2.5,1.7,2.5c-0.2,0-0.4,0.1-0.5,0.2L0.7,3.1C0.6,3.2,0.5,3.3,0.5,3.5c0,0.2,0.1,0.3,0.2,0.5l3.8,3.3
                C4.6,7.5,4.8,7.5,5,7.5c0.2,0,0.4-0.1,0.5-0.2L9.3,4c0.1-0.1,0.2-0.3,0.2-0.5C9.5,3.3,9.4,3.2,9.3,3.1L9.3,3.1z"/></g>
        </svg>`
    }
}

export default Svg
