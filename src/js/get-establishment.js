const isZone = ({ type }) => type === 'zone'

function getEstablishmentsOfZone(destination) {
    return isZone(destination)
        ? destination.children
        : destination
}

function findEstablishments(destinations) {
    const destinationsFlattened = destinations
        .flatMap(getEstablishmentsOfZone)

    if (destinationsFlattened.some(isZone)) {
        return findEstablishments(destinationsFlattened)
    }

    return destinationsFlattened
}

export function getEstablishment(destinations, id) {
    return findEstablishments(destinations)
        .find(establishment => establishment.id === id)
}
