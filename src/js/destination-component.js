import Fuse from 'fuse.js'
import Merge from 'deepmerge'
import Utils from './utils'
import Literals from './literals'
import OptionImage from './option-image'
import Svg from './svg'
import Translate from './Translate'
import { getZone } from './get-zone'
import { getEstablishment } from './get-establishment'

class DestinationComponent {
    constructor(selector, settings = {}) {
        this.element = document.querySelector(selector)
        // SELECTORS
        this.$ = {
            selector: null,
            dropdown: null,
            dropdownContainer: null,
            body: document.querySelector('body'),
        }

        // HELP VARS
        this.utils = {
            opened: false,
            search: '',
            width: 0,
            trans: null,
            // DROPDOWN HANDLER
            eventDropdownHandler: this.eventBindedDropdown.bind(this),
            eventPressEscHandler: this.eventBindedPressEsc.bind(this),
        }

        const defaults = {
            locale: {
                language: 'en',
                languagesAllowed: [ 'en', 'es', 'ca', 'de', 'fr', 'ru', 'it', 'nl' ],
            },
            singleDestination: false,
            appearance: {
                search: false,
                mode: 'columns',
                orientation: 'auto', // Auto, top, bottom
                breakpoints: {
                    one: 400,
                    two: 800,
                },
            },
            destinations: [],
            literals: Literals,
            selected: {
                id: null,
                type: null,
            },
        }
        this.settings = Merge(defaults, settings)
        this.check()
        this.init()
    }

    check() {
        if (!this.hasLevels()) {
            throw new Error('No destinations')
        }

        if (!this.checkLanguages()) {
            console.info(`${this.settings.locale.language} is not allowed. Setted 'en' instead`)
            this.settings.locale.language = 'en'
        }

        return this
    }

    init() {
        this.setLanguage()
            .preapreKeys()
            .createMainElement()
            .appendArrow()
            .appendDropdown()
            .appendMobileSettings()
            .appendSearch()
            .appendContainer()
            .appendOptions()
            .dropdownEventListenner()

        if (this.settings.selected) {
            this.customEvents()
        }
    }

    preapreKeys() {
        this.settings.destinations.forEach((destination) => {
            // eslint-disable-next-line
            if (destination.hasOwnProperty('establishment_type')) {
                // eslint-disable-next-line
                destination.translation_type = this.utils.trans.translate(destination.establishment_type, {num: 0})
                return
            }

            const lodgesTypes = []

            destination.children.forEach((zone) => {
                // eslint-disable-next-line
                if (zone.hasOwnProperty('establishment_type')) {
                    lodgesTypes.push(zone.establishment_type)
                    return
                }

                zone.children.forEach((element) => { lodgesTypes.push(element.establishment_type) })
            })

            // eslint-disable-next-line
            destination.translation_type = lodgesTypes.every((value, index, array) => value === array[0]) && lodgesTypes[0] !== null
                ? this.utils.trans.translate(lodgesTypes[0], {num: lodgesTypes.length})
                : this.utils.trans.translate('lodge', {num: lodgesTypes.length})

            // eslint-disable-next-line
            destination.childLength = lodgesTypes.length
        })

        return this
    }

    hasLevels() {
        return (this.settings.destinations.length)
    }

    checkLanguages() {
        // eslint-disable-next-line
        return this.settings.locale.languagesAllowed.includes(this.settings.locale.language)
    }

    setLanguage() {
        // eslint-disable-next-line
        this.utils.trans = Translate.instance
        return this
    }


    createMainElement() {
        const { destinations } = this.settings
        const selected = this.settings.selected.type === 'zone'
            ? destinations.find(getZone.bind(null, this.settings.selected.id))
            : getEstablishment(destinations, this.settings.selected.id)

        const destinationName = typeof selected !== 'undefined' ? selected : {name:''}
        const name = destinationName.hasOwnProperty('translations') ? destinationName.translations[this.settings.locale.language].name : destinationName.name
        const placeholder = destinationName.name !== ''
            ? name
            : this.utils.trans.translate('destination', {num: this.settings.destinations.length})

        const element = Utils.h('div', {
            id: `destination-component-${Utils.createRandomId()}`,
            class: `${this.element.className} ${this.settings.singleDestination === true ? 'destination-component-single' : ''}`,
        }, Utils.h('div', { class: 'destination-component_placeholder' }, placeholder))
        this.element.parentNode.insertBefore(element, this.element.nextSibling)
        this.element.style.display = 'none'
        this.$.selector = element
        return this
    }

    appendDropdown() {
        const position = Utils.position(this.$.selector)
        // eslint-disable-next-line
        const hasLevels = this.settings.destinations[0].hasOwnProperty('children')
        this.utils.width = position.width - 2
        const dropDown = Utils.h('div', { class: `${hasLevels ? '' : 'one-level'} destination-component_dropdown` })
        this.$.dropdown = dropDown
        this.$.selector.append(dropDown)
        return this
    }

    appendOrientationClass() {
        if (this.settings.appearance.orientation === 'auto' && this.orientation() === 'top') {
            this.addTopOrientation()
            return
        }

        if (this.settings.appearance.orientation === 'auto' && this.orientation() === 'bottom') {
            this.addBottomOrientation()
            return
        }

        if (this.settings.appearance.orientation === 'top') {
            this.addTopOrientation()
            return
        }

        if (this.settings.appearance.orientation === 'bottom') {
            this.addBottomOrientation()
        }
    }

    addTopOrientation() {
        this.$.dropdown.classList.remove('destination-component_dropdown--bottom')
        this.$.dropdown.classList.add('destination-component_dropdown--top')
    }

    addBottomOrientation() {
        this.$.dropdown.classList.remove('destination-component_dropdown--top')
        this.$.dropdown.classList.add('destination-component_dropdown--bottom')
    }

    orientation() {
        const position = Utils.position(this.$.selector)
        const height = window.innerHeight
        if (position.top + position.height > height / 2) {
            return 'top'
        }

        return 'bottom'
    }

    appendMobileSettings() {
        if (Utils.isMobile()) {
            this.$.selector.classList.add('destination-component--mobile')
            this.appendHeaderMobile()
            return this
        }

        this.$.dropdown.style.width = `${this.utils.width}px`
        return this
    }

    appendHeaderMobile() {
        const header = Utils.h('div', { class: 'destination-component-header' })
        header.innerHTML = `${this.utils.trans.translate('destination', {num: 2})} ${Svg.closeSVG()}`
        this.$.dropdown.prepend(header)
        this.addCloseListenner()
    }

    addCloseListenner() {
        document.querySelector('.destination-component-close').addEventListener('click', (ev) => {
            this.utils.opened = false
            this.unBindWindowEvent()
            this.hideDropdown()
            this.$.body.classList.remove('destination-component--opened')
        }, false)
    }

    appendSearch() {
        const options = {
            threshold: 0.3,
            minMatchCharLength: 3,
            keys: [ 'name', 'children.name', 'children.children.name' ],
        }
        this.settings.destinations = new Fuse(this.settings.destinations, options)
        if (this.settings.appearance.search) {
            const input = Utils.h('input')
            const search = Utils.h('div', { class: 'destination-component_search-container' }, input)
            this.searchEventListenner(input)
            this.$.dropdown.append(search)
            return this
        }
        return this
    }

    appendContainer() {
        const container = Utils.h('div', { class: 'destination-component_container' })
        if (Utils.isMobile()) {
            container.style['max-height'] = `${window.innerHeight - (this.calcHeaderSize() + 20)}px`
        }
        this.$.dropdownContainer = container
        this.$.dropdown.append(container)
        return this
    }

    calcHeaderSize() {
        const header = this.$.selector.querySelector('.destination-component-header')
        const search = this.$.selector.querySelector('.destination-component_search-container')
        if (search !== null) {
            return search.clientHeight + header.clientHeight
        }
        return header.clientHeight
    }

    searchEventListenner(input) {
        input.addEventListener('input', (ev) => {
            this.utils.search = ev.target.value
            this.$.dropdownContainer.innerHTML = ''
            this.appendImageOptions()
        }, false)
    }

    appendOptions() {
        this.appendImageOptions()
        if (this.settings.appearance.mode === 'columns') {
            this.addClassColumns()
            return this
        }

        this.$.selector.classList.add('destination-component_list')

        return this
    }

    addClassColumns() {
        this.$.dropdown.classList.add('has-image')

        if (this.utils.width > this.settings.appearance.breakpoints.two) {
            this.$.dropdown.classList.add('three-columns')
            return
        }
        // eslint-disable-next-line
        if (this.utils.width > this.settings.appearance.breakpoints.one && this.utils.width < this.settings.appearance.breakpoints.two) {
            this.$.dropdown.classList.add('two-columns')
            return
        }

        this.$.dropdown.classList.add('one-column')
    }

    appendArrow() {
        const svg = document.createElement('svg')
        svg.innerHTML = Svg.arrowDown()
        this.$.selector.append(svg)

        return this
    }

    appendImageOptions() {
        // eslint-disable-next-line
        const searchList = (this.utils.search !== '') ? this.settings.destinations.search(this.utils.search) : this.settings.destinations.list
        searchList.forEach((option) => {
            const optionObject = OptionImage.option(option, this.settings)
            this.bindEventOption(optionObject)
            this.$.dropdownContainer.append(optionObject)
        })
        if (searchList.length === 0) {
            const noResults = Utils.h('div', { class: 'destination-component_no_result' }, this.utils.trans.translate('no_results', { search: this.utils.search }))
            this.$.dropdownContainer.append(noResults)
        }
        return this
    }

    bindEventOption(optionObject) {
        optionObject.addEventListener('click', (ev) => {
            this.chooseOption(ev)
        }, false)
    }

    chooseOption(ev) {
        const element = Utils.isChild(ev.target, 'data-name', 'selector')
        if (element === null) {
            return
        }
        this.manageActiveClasses(ev.target)
        this.$.selector.querySelector('.destination-component_placeholder').textContent = element.getAttribute('data-name')
        this.settings.selected.id = element.getAttribute('data-id')
        this.settings.selected.type = element.getAttribute('data-type')
        this.settings.selected.name = element.getAttribute('data-name')
        this.hideDropdown()
        this.$.body.classList.remove('destination-component--opened')
        this.utils.opened = false
        this.unBindWindowEvent()
        this.customEvents()
    }

    manageActiveClasses(target) {
        const elements = this.$.selector.querySelectorAll('.active')
        if (elements.length) {
            elements.forEach((element) => {
                element.classList.remove('active')
            })
        }
        target.classList.add('active')
    }

    dropdownEventListenner() {
        this.$.selector.addEventListener('click', (ev) => {
            if (this.utils.opened && ev.target === this.$.selector) {
                this.hideDropdown()
                this.unBindWindowEvent()
                this.utils.opened = false
                return
            }
            this.appendOrientationClass()
            this.showDropdown()
            this.windowEvent()
            if (this.settings.appearance.search && !Utils.isMobile()) {
                this.$.selector.querySelector('.destination-component_search-container input').focus()
            }
            this.utils.opened = true
        }, true)

        return this
    }

    windowEvent() {
        document.addEventListener('click', this.utils.eventDropdownHandler, true)
        document.addEventListener('keydown', this.utils.eventPressEscHandler, true)
    }

    unBindWindowEvent() {
        document.removeEventListener('click', this.utils.eventDropdownHandler, true)
        document.removeEventListener('keydown', this.utils.eventPressEscHandler, true)
    }

    eventBindedDropdown(ev) {
        if (this.$.selector.contains(ev.target)) {
            return
        }
        this.hideDropdown()
        this.utils.opened = false
        this.unBindWindowEvent()
    }

    eventBindedPressEsc(ev) {
        if (ev.keyCode !== 27) {
            return
        }

        this.hideDropdown()
        this.utils.opened = false
        this.unBindWindowEvent()
    }

    showDropdown() {
        if (Utils.isMobile()) {
            this.$.body.classList.add('destination-component--opened')
        }
        this.$.selector.classList.add('destination-component_dropdown_show')
        return this
    }

    hideDropdown() {
        this.$.selector.classList.remove('destination-component_dropdown_show')
        return this
    }

    customEvents() {
        const event = new CustomEvent('selector:changed', {
            detail: this.settings.selected,
        })
        this.$.selector.dispatchEvent(event)
        return this
    }

    selector() {
        return this.$.selector
    }

    isValid(destination) {
        const target = this.$.selector.querySelector(`[data-id="${destination.id}"][data-type="${destination.type}"]`)
        return target !== null
    }

    setDestination(destination) {
        const target = this.$.selector.querySelector(`[data-id="${destination.id}"][data-type="${destination.type}"]`)
        if (!this.isValid(destination)) {
            console.error('Destination invalid')
            return
        }
        this.settings.selected = {
            id: destination.id,
            type: destination.type,
        }
        this.$.selector.querySelector('.destination-component_placeholder').textContent = target.getAttribute('data-name')
        this.manageActiveClasses(target)
        this.customEvents()
    }
}


export default DestinationComponent
