const singleton = Symbol()
const singletonEnforcer = Symbol()

class Translate {

    constructor (enforcer) {
        if (enforcer !== singletonEnforcer) {
            throw 'Cannot construct Translator singleton'
        }
    }

    static get instance () {
        if (!this[singleton]) {
            this[singleton] = new Translate(singletonEnforcer)
        }
        return this[singleton]
    }

    translate (key, params){
        let translation = key.split('.')
            .reduce((t, i) => {

                if (t === null) {
                    return null
                }

                return t[i] || null

            }, window.i18n_widget
            )

        
        if (translation.includes('|')) {
            translation = params.num && params.num > 1 ? translation.split('|')[1] : translation.split('|')[0]
        }

        if (!translation){
            console.error('Missing key '+key)
            return key
        }

        for (const placeholder in params) {
            if (placeholder === 'num') {
                break
            }
            translation = translation.replace(`:${placeholder}`, params[placeholder])
        }

        return translation
    }

}

export default Translate