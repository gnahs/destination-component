import Svg from './svg'
import Utils from './utils'

class OptionImage {
    static option(option, settings) {
        return OptionImage.template(option, settings)
    }

    static template(option, settings) {
        // eslint-disable-next-line
        if (!option.hasOwnProperty('children')) {
            return OptionImage.imageTemplateOneLevel(option, settings)
        }
        // eslint-disable-next-line
        if (!option.children[0].hasOwnProperty('children')) {
            return OptionImage.imageTemplateTwoLevels(option, settings)
        }
        return OptionImage.imageTemplateThreeLevels(option, settings)
    }

    static lastLevelTemplate(option, settings) {
        const name = option.hasOwnProperty('translations') ? option.translations[settings.locale.language].name : option.name
        return Utils.h('div', {
            class: `${settings.selected.id === option.id && settings.selected.type === option.type ? 'active' : ''} destination-component_last_level_title`,
            'data-name': name,
            'data-type': option.type,
            'data-id': option.id,
        }, Utils.h('span', { class: 'destination-component_square' }), name)
    }

    static imageTemplateOneLevel(option, settings) {
        const child = OptionImage.lastLevelTemplate(option, settings)
        return Utils.h('div', { class: 'destination-component_option' }, child)
    }

    static imageTemplateTwoLevels(option, settings) {
        const children = option.children.map(item => OptionImage.lastLevelTemplate(item, settings))
        const name = option.hasOwnProperty('translations') ? option.translations[settings.locale.language].name : option.name
        const optionsInner = Utils.h('div', {
            class: 'destination-component_image',
            style: `background-image: url(${option.img})`,
            'data-name': name,
            'data-type': option.type,
            'data-id': option.id,
        }, Utils.h('div', { class: 'destination-component_title' }, name), Utils.h('div', { class: 'destination-component_subtitle' }, `${option.childLength} ${option.translation_type}`))
        return Utils.h('div', { class: 'destination-component_option' }, optionsInner, ...children)
    }

    static imageTemplateThreeLevels(option, settings) {
        const name = option.hasOwnProperty('translations') ? option.translations[settings.locale.language].name : option.name
        const mainLevel = Utils.h('div', {
            class: `${settings.selected.id === option.id && settings.selected.type === option.type ? 'active' : ''} destination-component_image`,
            style: `background-image: url(${option.img})`,
            'data-name': name,
            'data-id': option.id,
            'data-type': option.type,
        }, Utils.h('div', { class: 'destination-component_title' }, name), Utils.h('div', { class: 'destination-component_subtitle' }, `${option.childLength} ${option.translation_type}`))

        // eslint-disable-next-line
        const level = option.children.map((zone) => {
            const zoneName = option.hasOwnProperty('translations') ? option.translations[settings.locale.language].name : option.name
            // eslint-disable-next-line
            const zoneCHild = Utils.h('div', { class: `destination-component_zone_level` },
                Utils.h('div', {
                    class: `destination-component_zone_level_title ${settings.selected.id === zone.id && settings.selected.type === zone.type ? 'active' : ''}`,
                    'data-name': zoneName,
                    'data-type': zone.type,
                    'data-id': zone.id,
                }, zoneName),
                Utils.h('div', { class: 'destination-component_last_level' }, ...zone.children.map((item, i) => OptionImage.lastLevelTemplate(item, settings))))

            return zoneCHild
        })

        return Utils.h('div', { class: 'destination-component_option' }, mainLevel, ...level)
    }
}

export default OptionImage
